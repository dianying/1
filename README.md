# [电影先生APP下载⏬](http://www.dianyingim.com/doc/app) #
下载提示：苹果APP客户端暂未开放下载，请收藏电影先生域名到浏览器，或添加至桌面图标。

[![app-download-android.png](https://bitbucket.org/repo/a567zzR/images/533083628-app-download-android.png)](https://at.umtrack.com/jaiSzC?cid=483)


安卓手机扫码下载

【[下载安卓APK(国内节点)⏬](https://at.umtrack.com/jaiSzC?cid=483)】

【[下载安卓APK(海外节点)⏬](https://at.umtrack.com/5HTrWv?cid=483)】


* <b>防封</b>：如果域名被封无法访问，可通过App打开、查看最新网址；
* <b>最新</b>：最新热门VIP在线看与下载资源同步更新；
* <b>多源</b>：持续添加优质线路、视频源供您切换，画质清晰、播放流畅；
* <b>海量</b>：聚合全网资源，想看啥就看啥
* <b>下载</b>：聚合全网下载，持续完善资源库

------------------------------------------

# 电影先生最新地址 #
近期，电影先生遭到不同程度的封锁屏蔽，导致部分地区无法访问。
以下方式均可找到电影先生备用网址，强烈建议截屏/保存。


# 域名列表 #
<p>
    <b><a href="https://at.umtrack.com/zC8H5f?cid=483" target="_blank">&gt;&gt;&gt;&gt;请收藏本页到浏览器&lt;&lt;&lt;&lt;</a></b><br>
</p>

### 5月2日最新线路  ：
[http://dyxs11.com](http://dyxs11.com)

[http://dyxs12.com](http://dyxs12.com)

[http://dyxs13.com](http://dyxs13.com)

[http://dyxs14.com](http://dyxs14.com)

[http://dyxs15.com](http://dyxs15.com)

### 官方域名  ：
[http://DianYingim.com](http://DianYingim.com)

[http://DianYing.im](http://DianYing.im)

[http://DianYing.in](http://DianYing.in)

如果您认可电影先生，请分享给你的好友！